package com.example.asadg.duaapp2.dua;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.example.asadg.duaapp2.LanguageChangeListenerActivity;
import com.example.asadg.duaapp2.R;
import com.example.asadg.duaapp2.dualists.PersonalDuaStore;
import com.example.asadg.duaapp2.home.Category;

import java.util.List;

public class DuaViewActivity extends LanguageChangeListenerActivity {

    private static final String ARG_CATEGORY = "DuaViewActivity.extra.category";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dua_view);
        final ViewPager pager = findViewById(R.id.dua_pager);
        final Category category = (Category) getIntent().getExtras().getSerializable(ARG_CATEGORY);
        pager.setAdapter(new DuaPageAdapter(getSupportFragmentManager(), category.getDuas()));
        findViewById(R.id.dua_bottom_bar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Add current dua to learned list
                PersonalDuaStore.INSTANCE.addLearned(category.getDuas().get(pager.getCurrentItem()));

                //Add current dua to favorite list
                PersonalDuaStore.INSTANCE.addFavorite(category.getDuas().get(pager.getCurrentItem()));

                //PLay Audio for current dua
                MediaPlayer player = MediaPlayer.create(DuaViewActivity.this, category.getDuas().get(pager.getCurrentItem()).getAudio());
                player.start();
            }
        });

    }

    @Override
    public void onLanguageChanged() {
        //TODO Handle Language changes
    }

    public static Intent newIntent(Activity homeActivity, Category category) {
        Intent intent = new Intent(homeActivity, DuaViewActivity.class);
        intent.putExtra(ARG_CATEGORY, category);
        return intent;
    }

    public static class DuaPageAdapter extends FragmentStatePagerAdapter {

        private List<Dua> duas;

        DuaPageAdapter(FragmentManager fm, List<Dua> duaList) {
            super(fm);
            duas = duaList;
        }

        @Override
        public Fragment getItem(int position) {
            return DuaViewFragment.newInstance(duas.get(position));
        }

        @Override
        public int getCount() {
            return duas.size();
        }
    }
}
