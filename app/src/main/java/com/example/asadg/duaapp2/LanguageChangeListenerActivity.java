package com.example.asadg.duaapp2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;

import static com.example.asadg.duaapp2.Receivers.LANGUAGE_CHANGED;

public abstract class LanguageChangeListenerActivity extends AppCompatActivity {
    public abstract void onLanguageChanged();

    private BroadcastReceiver languageChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                switch (intent.getAction()) {
                    case LANGUAGE_CHANGED:
                        onLanguageChanged();
                        break;
                }
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(languageChangeReceiver, new IntentFilter(LANGUAGE_CHANGED));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(languageChangeReceiver);
    }
}
