package com.example.asadg.duaapp2.home;

import com.example.asadg.duaapp2.R;
import com.example.asadg.duaapp2.dua.Dua;

import java.util.ArrayList;
import java.util.List;

public class CategoryStore {
    public static final CategoryStore INSTANCE = new CategoryStore();

    private List<Category> categories = new ArrayList<>();

    private CategoryStore() {
    }

    public List<Category> getCategories() {
        if (categories.isEmpty()) {
            loadCategories();
        }
        return categories;
    }

    private void loadCategories() {

        //Todo Add more categories here
        categories.add(getMorningDuasCategory());

    }


    private Category getMorningDuasCategory() {
        Category category = new Category("Morning", "dada", R.drawable.ic_launcher_background);
        category.setDuas(getMorningDuas());
        return category;
    }

    private List<Dua> getMorningDuas() {
        //TODO make the listing of duas complete
        List<Dua> duas = new ArrayList<>();
        duas.add(new Dua(R.xml.acc_sup_dua3, R.raw.a2));
        return duas;
    }
}