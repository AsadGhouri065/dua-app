package com.example.asadg.duaapp2.dua;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.asadg.duaapp2.R;
import com.example.asadg.duaapp2.settings.AppSettings;

public class DuaViewFragment extends Fragment {
    private static final String ARG_DUA = "DuaViewFragment.arg.dua";
    private Dua currentDua;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dua_fragment_view, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        currentDua = (Dua) getArguments().getSerializable(ARG_DUA);
        TextView titleView = view.findViewById(R.id.dua_view_title);
        TextView contentView = view.findViewById(R.id.dua_view_content);
        contentView.setText(currentDua.getContent());
        titleView.setText(currentDua.getTitle(AppSettings.INSTANCE.getLanguage()));
    }

    public static Fragment newInstance(Dua dua) {
        Fragment fragment = new DuaViewFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_DUA, dua);
        fragment.setArguments(bundle);
        return fragment;
    }
}
