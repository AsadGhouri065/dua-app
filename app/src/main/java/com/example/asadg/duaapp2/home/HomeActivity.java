package com.example.asadg.duaapp2.home;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.asadg.duaapp2.LanguageChangeListenerActivity;
import com.example.asadg.duaapp2.R;
import com.example.asadg.duaapp2.dua.DuaViewActivity;

public class HomeActivity extends LanguageChangeListenerActivity implements CategoryClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        CategoryAdapter adapter = new CategoryAdapter(CategoryStore.INSTANCE.getCategories(), this);
        RecyclerView recyclerView = findViewById(R.id.home_category_list);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onLanguageChanged() {
        //NotifyAdapterDataSetChanged
    }

    @Override
    public void onCategoryClicked(Category category) {
        startActivity(DuaViewActivity.newIntent(this, category));
    }
}
