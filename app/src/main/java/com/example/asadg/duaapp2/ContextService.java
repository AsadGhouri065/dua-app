package com.example.asadg.duaapp2;

import android.content.Context;

public class ContextService {

    private Context appContext;

    public static final ContextService INSTANCE = new ContextService();

    private ContextService() {
    }

    public Context getAppContext() {
        return appContext;
    }

    public void setAppContext(Context appContext) {
        this.appContext = appContext;
    }
}
