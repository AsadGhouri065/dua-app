package com.example.asadg.duaapp2.dualists;

import com.example.asadg.duaapp2.R;
import com.example.asadg.duaapp2.dua.Dua;

import java.util.ArrayList;
import java.util.List;

public class PersonalDuaStore {

    private List<Dua> favoriteDuas = new ArrayList<>();
    private List<Dua> learnedDuas = new ArrayList<>();


    public static final PersonalDuaStore INSTANCE = new PersonalDuaStore();

    private PersonalDuaStore(){}

    //Returns the list of duas liked by the user
    public List<Dua> getFavoriteDuas() {
        if(favoriteDuas.isEmpty()){
            loadFavorites();
        }
        return favoriteDuas;
    }

    //Use this to add a dua to liked list of duas
    public void addFavorite(Dua dua){
        favoriteDuas.add(dua);
        saveFavorite(dua);
    }

    private void saveFavorite(Dua dua) {
        //TODO actually store data in some preference
    }

    public void addLearned(Dua dua){
        learnedDuas.add(dua);
        saveLearned(dua);
    }

    private void saveLearned(Dua dua) {
        //TODO actually store data in some preference
    }

    private void loadFavorites() {
        //TODO add a way to load actual preference of user
      //  favoriteDuas.add(new Dua(R.xml.acc_sup_dua1, R.raw.a0));
    }

    public List<Dua> getLearnedDuas() {
        if(learnedDuas.isEmpty()){
            loadLearned();
        }
        return learnedDuas;
    }

    private void loadLearned() {
        //TODO add a way to load actual preference of user
        //learnedDuas.add(new Dua(R.xml.acc_sup_dua2, R.raw.a1));
    }
}
