package com.example.asadg.duaapp2.home;

interface CategoryClickListener {
    void onCategoryClicked(Category category);
}
