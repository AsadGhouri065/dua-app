package com.example.asadg.duaapp2.home;

import android.support.annotation.DrawableRes;

import com.example.asadg.duaapp2.Language;
import com.example.asadg.duaapp2.dua.Dua;

import java.io.Serializable;
import java.util.List;

public class Category implements Serializable {
    private String nameInEnglish;
    private String nameInUrdu;
    private int icon;
    private List<Dua> duas;

    public Category(String nameInEnglish, String nameInUrdu, @DrawableRes int icon) {
        this.nameInEnglish = nameInEnglish;
        this.nameInUrdu = nameInUrdu;
        this.icon = icon;
    }

    public String getDisplayName(Language language) {
        switch (language) {
            case URDU:
                return nameInUrdu;
            case ENGLISH:
            default:
                return nameInEnglish;
        }
    }

    public int getIcon() {
        return icon;
    }

    public List<Dua> getDuas() {
        return duas;
    }

    public void setDuas(List<Dua> duas) {
        this.duas = duas;
    }
}
