package com.example.asadg.duaapp2.home;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.asadg.duaapp2.R;
import com.example.asadg.duaapp2.settings.AppSettings;

import java.util.List;

class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    private List<Category> categories;
    private CategoryClickListener categoryListener;

    CategoryAdapter(List<Category> categories, CategoryClickListener listener) {
        this.categories = categories;
        categoryListener = listener;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_item_cell, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryViewHolder holder, int position) {
        //TODO Define view mappings
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoryListener.onCategoryClicked(categories.get(holder.getAdapterPosition()));
            }
        });
        holder.textView.setText(categories.get(holder.getAdapterPosition()).getDisplayName(AppSettings.INSTANCE.getLanguage()));
        holder.image.setImageResource(categories.get(holder.getAdapterPosition()).getIcon());

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView textView;

        CategoryViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.category_cell_icon);
            textView = itemView.findViewById(R.id.category_cell_title);
        }
    }
}
