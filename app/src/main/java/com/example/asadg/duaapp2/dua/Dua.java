package com.example.asadg.duaapp2.dua;

import android.content.res.XmlResourceParser;
import android.util.Log;

import com.example.asadg.duaapp2.ContextService;
import com.example.asadg.duaapp2.Language;

import org.xmlpull.v1.XmlPullParser;

import java.io.Serializable;

public class Dua implements Serializable {
    private int duaResource;
    private int duaAudio;
    private ParsedDua cachedDua;

    public Dua(int duaResource, int duaAudio) {
        this.duaResource = duaResource;
        this.duaAudio = duaAudio;
    }

    public int getAudio() {
        return duaAudio;
    }

    public String getContent() {
        return getParsedDua().getArabicContent();
    }

    public String getTitle(Language language) {
        ParsedDua dua = getParsedDua();

        switch (language) {
            case URDU:
                return dua.getTitleUrdu();
            case ENGLISH:
            default:
                return dua.getTitleEnglish();
        }
    }

    public String getTranslation(Language language) {
        ParsedDua dua = getParsedDua();

        switch (language) {
            case URDU:
                return dua.getTranslationUrdu();
            case ENGLISH:
            default:
                return dua.getTranslationEnglish();
        }
    }

    public String getReference(Language language) {
        ParsedDua dua = getParsedDua();

        switch (language) {
            case URDU:
                return dua.getReferenceUrdu();
            case ENGLISH:
            default:
                return dua.getReferenceEnglish();
        }
    }

    public String getBenefits(Language language) {
        ParsedDua dua = getParsedDua();
        switch (language) {
            case URDU:
                return dua.getBenefitsUrdu();
            case ENGLISH:
            default:
                return dua.getBenefitsEnglish();
        }
    }

    public String getTime() {
        return getParsedDua().getTime();
    }

    private ParsedDua getParsedDua() {
        if (cachedDua == null) {
            cachedDua = parseDua();
        }
        return cachedDua;
    }

    private ParsedDua parseDua() {
        ParsedDua parsedData = new ParsedDua();
        try {
            XmlResourceParser parser = ContextService.INSTANCE.getAppContext().getResources().getXml(duaResource);
            parser.next();
            int event = parser.getEventType();
            String lastKey = "";
            do {
                if (event == XmlPullParser.START_TAG) {
                    String tag = parser.getName();
                    switch (tag) {
                        case "key":
                            if (parser.next() == XmlPullParser.TEXT) {
                                lastKey = parser.getText();
                            }
                            break;
                        case "string":
                            if (parser.next() == XmlPullParser.TEXT) {
                                updateValue(parsedData, lastKey, parser.getText());
                                lastKey = "";
                            }
                            break;
                    }
                }
                parser.next();
                event = parser.getEventType();
            } while (event != XmlPullParser.END_DOCUMENT);
        } catch (Exception e) {
            Log.e("DUA APPLICATION XML", e.toString());
        }
        return parsedData;
    }


    private void updateValue(ParsedDua parsedData, String lastKey, String text) {
        switch (lastKey) {
            case "Title_urdu":
                parsedData.titleUrdu = text;
                break;
            case "Title_eng":
                parsedData.titleEnglish = text;
                break;
            case "Reference_eng":
                parsedData.referenceEnglish = text;
                break;
            case "Reference_urdu":
                parsedData.referenceUrdu = text;
                break;
            case "Eng_trans":
                parsedData.translationEnglish = text;
                break;
            case "Urdu_trans":
                parsedData.translationUrdu = text;
                break;
            case "Merits_eng":
                parsedData.benefitsEnglish = text;
                break;
            case "Merits_urdu":
                parsedData.benefitsUrdu = text;
                break;
            case "Dua_arabic":
                parsedData.arabicContent = text;
                break;
            case "Time":
                parsedData.time = text;
                break;
        }
    }


    private static class ParsedDua {
        private String titleUrdu;
        private String titleEnglish;
        private String translationUrdu;
        private String translationEnglish;
        private String referenceUrdu;
        private String referenceEnglish;
        private String benefitsUrdu;
        private String benefitsEnglish;
        private String time;
        private String arabicContent;

        String getTitleUrdu() {
            return titleUrdu;
        }

        String getTitleEnglish() {
            return titleEnglish;
        }

        String getTranslationUrdu() {
            return translationUrdu;
        }

        String getTranslationEnglish() {
            return translationEnglish;
        }

        String getReferenceUrdu() {
            return referenceUrdu;
        }

        String getReferenceEnglish() {
            return referenceEnglish;
        }

        String getBenefitsUrdu() {
            return benefitsUrdu;
        }

        String getBenefitsEnglish() {
            return benefitsEnglish;
        }

        String getTime() {
            return time;
        }

        String getArabicContent() {
            return arabicContent;
        }
    }
}