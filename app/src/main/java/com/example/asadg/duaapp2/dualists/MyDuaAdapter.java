package com.example.asadg.duaapp2.dualists;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.asadg.duaapp2.R;
import com.example.asadg.duaapp2.dua.Dua;

import java.util.List;

class MyDuaAdapter extends RecyclerView.Adapter<MyDuaAdapter.DuaViewHolder> {
    private List<Dua> duaList;

    MyDuaAdapter(List<Dua> duas) {
        duaList = duas;
    }

    @NonNull
    @Override
    public DuaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DuaViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_my_dua, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DuaViewHolder holder, int position) {
        //TODO Define how a row will be rendereed
        duaList.get(holder.getAdapterPosition());
    }

    @Override
    public int getItemCount() {
        return duaList.size();
    }

    class DuaViewHolder extends RecyclerView.ViewHolder {
        DuaViewHolder(View itemView) {
            super(itemView);
        }
    }
}
