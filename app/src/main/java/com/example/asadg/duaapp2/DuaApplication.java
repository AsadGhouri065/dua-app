package com.example.asadg.duaapp2;

import android.app.Application;

public class DuaApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ContextService.INSTANCE.setAppContext(getApplicationContext());
    }
}
