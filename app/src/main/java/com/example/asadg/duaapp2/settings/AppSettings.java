package com.example.asadg.duaapp2.settings;

import android.content.Intent;

import com.example.asadg.duaapp2.ContextService;
import com.example.asadg.duaapp2.Language;
import com.example.asadg.duaapp2.Receivers;

public class AppSettings {
    public static final AppSettings INSTANCE = new AppSettings();
    private static final int DEFAULT_FONT_SIZE = 20;

    private Language language;
    private int fontSize = DEFAULT_FONT_SIZE;

    private AppSettings(){
        language = Language.ENGLISH;
    }

    public Language getLanguage() {
        return language;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public void changeLanguage(Language language) {
        this.language = language;
        ContextService.INSTANCE.getAppContext().sendBroadcast(new Intent(Receivers.LANGUAGE_CHANGED));
    }
}
