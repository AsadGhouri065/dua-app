package com.example.asadg.duaapp2.dualists;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.asadg.duaapp2.R;
import com.example.asadg.duaapp2.dua.Dua;

import java.util.List;

public class MyDuaActivity extends AppCompatActivity {

    private static final String LIST_TYPE = "my.dua.page.type";
    public static final int FAVORITE_PAGE = 0;
    public static final int LEARNED_PAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_dua);
        RecyclerView recyclerView = findViewById(R.id.my_duas_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        int pageType = getIntent().getIntExtra(LIST_TYPE, 0);
        MyDuaAdapter adapter = new MyDuaAdapter(loadListBasedOnPage(pageType));
        recyclerView.setAdapter(adapter);
    }

    private List<Dua> loadListBasedOnPage(int pageType) {
        switch (pageType) {
            case FAVORITE_PAGE:
                return PersonalDuaStore.INSTANCE.getFavoriteDuas();
            case LEARNED_PAGE:
            default:
                return PersonalDuaStore.INSTANCE.getLearnedDuas();
        }
    }

    /*
    Usages:
        To Open Favorites:
        startActivity(MyDuaActivity.getIntent(this, MyDuaActivity.FAVORITE_PAGE);
        To Open Learned:
        startActivity(MyDuaActivity.getIntent(this, MyDuaActivity.LEARNED_PAGE);

    */
    public static Intent getIntent(Activity activity, int page) {
        return new Intent(activity, MyDuaActivity.class).putExtra(LIST_TYPE, page);
    }
}
